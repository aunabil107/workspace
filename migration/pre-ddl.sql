CREATE TABLE designation(
  id INT NOT NULL,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE user (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  designation_id INT NOT NULL,
  picture_url VARCHAR(50),
  phone_no VARCHAR(50),
  address VARCHAR(200),
  is_admin INT,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id),
  FOREIGN KEY (designation_id) REFERENCES designation(id)
);

CREATE TABLE post(
  id INT NOT NULL AUTO_INCREMENT,
  time DATETIME NOT NULL,
  title VARCHAR(1000),
  body VARCHAR(10000) NOT NULL,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id)
);

CREATE TABLE task(
  id INT NOT NULL AUTO_INCREMENT,
  deadline DATETIME NOT NULL,
  description VARCHAR(1000) NOT NULL,
  is_completed INT NOT NULL,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id)
);

CREATE TABLE meeting(
  id INT NOT NULL AUTO_INCREMENT,
  time DATETIME NOT NULL,
  title VARCHAR(100) NOT NULL,
  agenda VARCHAR(1000) NOT NULL,
  url VARCHAR(50) NOT NULL,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id)
);

CREATE TABLE team(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  teamlead_id INT NOT NULL,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id),
  FOREIGN KEY(teamlead_id) REFERENCES user(id)
);

CREATE TABLE post_file(
  post_id INT NOT NULL,
  url VARCHAR(50) NOT NULL,
  FOREIGN KEY (post_id) 
  REFERENCES post(id)
);

CREATE TABLE team_task(
  team_id INT NOT NULL,
  task_id INT NOT NULL,
  FOREIGN KEY (team_id) 
  REFERENCES team(id),
  FOREIGN KEY (task_id)
  REFERENCES task(id)
);

CREATE TABLE team_user(
  team_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (team_id) 
  REFERENCES team(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);

CREATE TABLE message(
  id INT NOT NULL AUTO_INCREMENT,
  time DATETIME NOT NULL,
  text VARCHAR(1000) NOT NULL,
  sender_user_id INT NOT NULL,
  receiver_user_id INT NOT NULL,
  created_date DATETIME,
  created_by INT,
  last_updated_date DATETIME,
  last_updated_by INT,
  deleted_date DATETIME,
  deleted_by INT,
  status INT,
  PRIMARY KEY (id),
  FOREIGN KEY (sender_user_id)
  REFERENCES user(id),
  FOREIGN KEY (receiver_user_id)
  REFERENCES user(id)
);

CREATE TABLE user_meeting(
  meeting_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (meeting_id) 
  REFERENCES meeting(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);
 		


CREATE TABLE user_assigned_task(
  task_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (task_id) 
  REFERENCES task(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);

CREATE TABLE user_created_task(
  task_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (task_id) 
  REFERENCES task(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);

CREATE TABLE user_created_post(
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (post_id)
  REFERENCES post(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);

CREATE TABLE user_tagged_post(
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  FOREIGN KEY (post_id) 
  REFERENCES post(id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
);

