# WorkSpace

We use gmail, google chat and google meets for our official
needs in Therap. It sometimes becomes difficult to keep track of all the
meetings, messages, emails and deadlines in this ecosystem.
So, keeping this in mind, we want to create a different ecosystem for our
office workspace using Spring and Hibernate JPA, called WorkSpace.
It can be used by any software company for maintaining day to day
development related task scheduling, setting up team meetings, contacts
and mails, all in one place.
